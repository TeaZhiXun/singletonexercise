﻿namespace SingletonExercise 
{
    class HighScore
    {
        private static readonly object _lock = new object();
        private static HighScore _instance;
        public string PlayerName { get; private set; }
        public int Score { get; private set; }
        public static HighScore Instance
        {

            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new HighScore();
                    }
                    return _instance;
                }
            }
        }
        public void UpdateHighScore(string name, int score)
        {
            if (score > Score) {
                Score = score;
                PlayerName = name;
            }
        }
    }
}