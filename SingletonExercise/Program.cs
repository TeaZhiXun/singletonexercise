﻿using System;

namespace SingletonExercise
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var highScore = HighScore.Instance;
            highScore.UpdateHighScore("Jack", 1000);

            //var highScore2 = new HighScore();

            Foo();
            Console.WriteLine($"High score: {highScore.PlayerName} {highScore.Score}");
        }
        public static void Foo()
        {
            var highScore = HighScore.Instance;
            highScore.UpdateHighScore("Jill", 2000);
        }
    }
}
